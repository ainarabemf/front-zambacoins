import AOS from 'aos'
import 'aos/dist/aos.css' // You can also use <link> for styles
// ..

// "async" is optional;
// more info on params: https://quasar.dev/quasar-cli/boot-files
export default async (/* { app, router, Vue ... } */) => {
  AOS.init()
}
