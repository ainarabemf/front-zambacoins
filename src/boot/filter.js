// import something here
import { format } from 'currency-formatter'
import moment from 'moment'
// "async" is optional
moment.locale('fr')
export default async ({ Vue }) => {
  Vue.filter('ariary', (value) => {
    return format(value, {
      symbol: 'Ar',
      decimal: ',',
      thousand: ' ',
      precision: 2,
      format: '%v %s'
    })
  })
  Vue.filter('dollar', (value) => {
    return format(value, {
      symbol: '$',
      decimal: ',',
      thousand: ' ',
      precision: 2,
      format: '%s %v'
    })
  })
  Vue.filter('fullDate', (value) => {
    return moment(value).format('DD MMMM YYYY')
  })
  Vue.filter('from', (value) => {
    return moment(value).fromNow()
  })
}
