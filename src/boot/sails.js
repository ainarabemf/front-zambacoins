import { LocalStorage } from 'quasar'
import socketIOClient from 'socket.io-client'
import sailsIOClient from 'sails.io.js'

// spécification l'hôte et le port du backend Sails pour que quasar puisse communiquer avec les API
var io = sailsIOClient(socketIOClient)

// connexion sur http://localhost:1337/
io.sails.url = 'http://localhost:1337/'

export { io }

const injectAuthToken = (options) => {
  const authToken = LocalStorage.getItem('auth_token')
  options.headers = { Authorization: 'Bearer ' + authToken }
  return options
}
export default async ({ Vue }) => {
  // something to do
  Vue.prototype.$socket = io.socket
}

export const IOWrapper = {
  /**
   * @desc wrapper of io.socket.get() which can inject the auth_token
   * @param {string} url
   * @param {function} cb
   * @param {bool} [authNeeded=true]
   */
  get: (url, cb, authNeeded = true) => {
    let options = {
      method: 'GET',
      url
    }

    if (authNeeded) {
      options = injectAuthToken(options)
    }

    io.socket.request(options, cb)
  },

  /**
   * @desc wrapper of io.socket.post() which can inject the auth_token
   * @param {string} url
   * @param {object} data
   * @param {function} cb
   * @param {bool} [authNeeded=true]
   */
  post: (url, data, cb, authNeeded = true) => {
    let options = {
      method: 'POST',
      url,
      data
    }

    if (authNeeded) {
      options = injectAuthToken(options)
    }
    io.socket.request(options, cb)
  },

  /**
   * @desc wrapper of io.socket.put() which can inject the auth_token
   * @param {string} url
   * @param {object} data
   * @param {function} cb
   * @param {bool} [authNeeded=true]
   */
  put: (url, data, cb, authNeeded = true) => {
    let options = {
      method: 'PUT',
      url,
      data
    }

    if (authNeeded) {
      options = injectAuthToken(options)
    }
    io.socket.request(options, cb)
  },

  /**
   * @desc wrapper of io.socket.delete() which can inject the auth_token
   * @param {string} url
   * @param {object} data
   * @param {function} cb
   * @param {bool} [authNeeded=true]
   */
  delete: (url, data, cb, authNeeded = true) => {
    let options = {
      method: 'DELETE',
      url,
      data
    }

    if (authNeeded) {
      options = injectAuthToken(options)
    }
    io.socket.request(options, cb)
  },

  /**
   * @desc wrapper of io.socket.on()
   * @param {string} eventIdentity
   * @param {function} cb
   */
  on: (eventIdentity, cb) => {
    io.socket.on(eventIdentity, cb)
  },

  /**
   * @desc wrapper of io.socket.off()
   * @param {string} eventIdentity
   * @param {function} cb
   */
  off: (eventIdentity, cb) => {
    io.socket.off(eventIdentity, cb)
  }
}
