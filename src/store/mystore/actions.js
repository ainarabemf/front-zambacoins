import { IOWrapper } from 'boot/sails'
import { LocalStorage } from 'quasar'
import axios from 'axios'
import qs from 'querystring'
export function login ({ commit }, data) {
  return new Promise((resolve, reject) => {
    IOWrapper.post('/auth/signin', data, (resp) => {
      console.log({ resp })
      commit('SET_USER_CONNECTED', resp)
      resolve(resp)
    }, false)
  })
}
export function loadPage ({ commit }) {
  commit('LOAD_PAGE')
}
export function signup ({ commit }, data) {
  return new Promise((resolve, reject) => {
    IOWrapper.post('/auth/signup', data, (resp) => {
      console.log({ resp })
      commit('SET_USER_CONNECTED', resp)
      resolve(resp)
    }, false)
  })
}
export function treeLevelFilleul ({ commit }, idUser) {
  return new Promise((resolve, reject) => {
    IOWrapper.get('/api/mutlilevel/' + idUser, (resp) => {
      console.log({ resp })
      commit('SET_FILLEULS', resp)
      resolve(resp)
    })
  })
}
export function getHomeUsers ({ commit }, number) {
  return new Promise((resolve, reject) => {
    IOWrapper.get('/api/users/' + number, (resp) => {
      console.log({ resp })
      resolve(resp)
    })
  })
}
export function logout ({ commit }) {
  LocalStorage.remove('user_auth')
  LocalStorage.remove('auth_token')
}
export function transactionEntrante ({ commit }, data) {
  axios.post('https://private-anon-579a5faac3-payeercom.apiary-proxy.com/ajax/api/api.php?transfer', qs.stringify(data.dataTrans), {
    headers: {
      'Content-type': 'application/x-www-form-urlencoded'
    }
  }).then((result) => {
    // Do somthing
    console.log(result)
    if (result.data.success) {
      IOWrapper.post('/api/transactionentrante/', data, (resp) => {
        commit('SET_USER_CONNECTED2', resp)
        console.log({ resp })
      })
    }
  })
}
