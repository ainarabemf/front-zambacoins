import { LocalStorage } from 'quasar'

export function SET_USER_CONNECTED (state, userConnected) {
  state.userConnected = userConnected.user
  LocalStorage.set('auth_token', userConnected.token)
  LocalStorage.set('user_auth', userConnected.user)
}
export function SET_USER_CONNECTED2 (state, userConnected) {
  state.userConnected = userConnected.user
  LocalStorage.set('user_auth', userConnected.user)
}
export function LOAD_PAGE (state) {
  state.userConnected = LocalStorage.getItem('user_auth')
}
export function SET_FILLEULS (state, filleuls) {
  state.filleuls = filleuls
}
