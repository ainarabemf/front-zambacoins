
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') }
    ]
  },
  {
    path: '/auth',
    component: () => import('layouts/LayoutLogin.vue'),
    children: [
      { path: '/register/:code', component: () => import('pages/Register.vue') },
      { path: '/login', component: () => import('pages/LoginPage.vue') }
    ]
  },
  {
    path: '/dashboard',
    component: () => import('layouts/Dashboard.vue'),
    children: [
      { path: '', component: () => import('pages/MainPage.vue') },
      { path: '/dashboard/filleuls', component: () => import('pages/AllFilleuls.vue') },
      { path: '/dashboard/depot', component: () => import('pages/Depot.vue') },
      { path: '/dashboard/story/incoming', component: () => import('pages/TransactionEntrante.vue') },
      { path: '/dashboard/story/outgoing', component: () => import('pages/TransactionSortante.vue') }
    ]
  },
  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
